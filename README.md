# Weather Server #

This project implements a network server for Davis Vantage Pro 2 weather stations and runs on Netduino Plus 2 hardware.

Hardware modifications to the Davis console are required, to bring out one of the serial ports that is present on the EXPANSION connector to COM1 on the Netduino.

This project builds upon the general purpose Network Command Protocol project.

Currently, this project is closed source and no license is available to any third parties.