using System;
using Microsoft.SPOT;

namespace TA.NetMF.WeatherServer
    {

    [Serializable]
    public class SerialReadTimeoutException : Exception
        {
        public Timeout Timeout { get; private set; }

        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //


        public SerialReadTimeoutException() : base("Serial read operation timed out.") {}

        public SerialReadTimeoutException(string message) : base(message) {}

        public SerialReadTimeoutException(string message, Exception inner) : base(message, inner) {}

        public SerialReadTimeoutException(Timeout timeout)
            : base("Serial read timed out after " + timeout.ToString())
            {
            this.Timeout = timeout;
            }
        public SerialReadTimeoutException(Timeout timeout, Exception inner)
            : base("Serial read timed out after " + timeout.ToString(), inner)
            {
            this.Timeout = timeout;
            }
        }
    }
