// This file is part of the TA.NetMF.WeatherServer project
// 
// Copyright � 2015 Tigra Networks., all rights reserved.
// 
// File: DavisVantagePro2WeatherConsole.cs  Last modified: 2015-07-08@19:15 by Tim Long

using System;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Threading;
using TA.NetMF.WeatherServer.Diagnostics;

namespace TA.NetMF.WeatherServer
    {
    public class DavisVantagePro2WeatherConsole : IWeatherConsole
        {
        readonly SerialPort serialPort;
        readonly int bufferSize;
        int bufferHead;
        byte[] receiveBuffer;
        readonly ManualResetEvent dataReceivedSignal = new ManualResetEvent(false);
        static readonly Timeout WakeupTimeout = Timeout.FromMilliseconds(1200);
        DateTime timeOfLastOperation = DateTime.MinValue;
        static readonly Timeout consoleSleepTime = Timeout.FromMilliseconds(120000); // 2 minutes

        /// <summary>
        ///     Initializes a new instance of the <see cref="DavisVantagePro2WeatherConsole" /> class.
        /// </summary>
        /// <param name="port">The ready-configured serial port to be used for communications.</param>
        public DavisVantagePro2WeatherConsole(SerialPort port, int bufferSize = 4096)
            {
            serialPort = port;
            this.bufferSize = bufferSize;
            }

        public static SerialPort GetConsoleSerialPort(string portName)
            {
            var port = new SerialPort(portName, 19200, Parity.None, 8, StopBits.One);
            port.ReadTimeout = Timeout.FromMilliseconds(100);
            return port;
            }

        public void Open()
            {
            receiveBuffer = new byte[bufferSize];
            bufferHead = 0;
            serialPort.DataReceived += SerialPortOnDataReceived;
            serialPort.Open();
            }

        void SerialPortOnDataReceived(object sender, SerialDataReceivedEventArgs serialDataReceivedEventArgs)
            {
            lock (receiveBuffer)
                {
                var bytesToRead = serialPort.BytesToRead;
                var bufferAvailable = bufferSize - bufferHead;
                if (bytesToRead > bufferAvailable)
                    throw new IOException("Serial read buffer overflow. Consider increasing the buffer size");
                var bytesReceived = serialPort.Read(receiveBuffer, bufferHead, bufferAvailable);
                bufferHead += bytesReceived;
                Dbg.Trace("Received " + bytesReceived + " bytes, " + bufferHead + " bytes in buffer", Source.SerialPort);
                }
            dataReceivedSignal.Set();
            }

        public void Close()
            {
            serialPort.DataReceived -= SerialPortOnDataReceived;
            serialPort.Close();
            }

        /// <summary>
        ///     Try up to 3 times to wake up the console, which can take up to 1.2 seconds per attempt.
        /// </summary>
        /// <exception cref="IOException">Unable to wake up the console</exception>
        public void Wakeup()
            {
            TimestampLastOperation();
            int tryNumber = 1;
            int triesAllowed = 3;
            while (tryNumber <= triesAllowed)
                {
                Dbg.Trace("Wakeup - try " + tryNumber, Source.WeatherConsole);
                ResetBuffers();
                serialPort.WriteByte(0x0A); // Line Feed <LF>
                var signalled = dataReceivedSignal.WaitOne(WakeupTimeout, false);
                if (signalled)
                    {
                    if (receiveBuffer[0] == 0x0A && receiveBuffer[1] == 0x0D)
                        {
                        Dbg.Trace("Wakeup successful", Source.WeatherConsole);
                        return; // Success!
                        }
                    Dbg.Trace("Console responded with unexpected data", Source.WeatherConsole);
                    }
                else
                    {
                    Dbg.Trace("Console did not respond", Source.WeatherConsole);
                    }
                ++tryNumber;
                }
            Dbg.Trace("Wakeup failed after " + triesAllowed + " attempts; throwing IOException.", Source.WeatherConsole);
            throw new IOException("Unable to wake up the console");
            }

        /// <summary>
        ///     Receives a multi line response from the station directly into a <see cref="ResponseBuilder" />. Data is
        ///     received until there has been nothing received for <paramref name="quietTime" />.
        /// </summary>
        /// <param name="responseBuilder">The response builder.</param>
        /// <param name="quietTime">
        ///     The quiet time that must elapse before the response is considered to have ended.
        /// </param>
        /// <returns>ResponseBuilder.</returns>
        public ResponseBuilder ReceiveResponse(ResponseBuilder responseBuilder, Timeout quietTime)
            {
            bool receiving = true;
            var lineBuilder = new StringBuilder();
            int lineCounter = 0;
            while (receiving)
                {
                receiving = dataReceivedSignal.WaitOne(quietTime, false);
                Dbg.Trace("Signalled", Source.WeatherConsole);
                if (receiving)
                    {
                    var utf8 = GetReceivedBytesAsUtf8();
                    dataReceivedSignal.Reset();
                    /*
                     * Take all the bytes received so far, convert to UTF-8 characters.
                     * Then break the character stream into a series of lines, each of which is added
                     * to the response as a new payload item.
                     * This continues until no data has been received for at least quietTime.
                     */
                    for (int index = 0; index < utf8.Length; index++)
                        {
                        char c = utf8[index];
                        if (c == '\x0A' || c == '\x0D')
                            {
                            // End of line detected, add a response line to the responseBuilder.
                            lineCounter = AddLineToResponse(responseBuilder, lineCounter, lineBuilder);
                            }
                        else
                            {
                            lineBuilder.Append(c);
                            }
                        }
                    }
                else
                    {
                    // handle whatever is left over in lineBuilder
                    lineCounter = AddLineToResponse(responseBuilder, lineCounter, lineBuilder);
                    }
                }
            return responseBuilder;
            }

        /// <summary>
        ///     Adds the line from the line builder to the response builder as a new payload item.
        /// </summary>
        /// <param name="responseBuilder">The response builder.</param>
        /// <param name="lineCounter">The line counter.</param>
        /// <param name="lineBuilder">The line builder.</param>
        /// <returns>System.Int32, the updated line counter.</returns>
        static int AddLineToResponse(ResponseBuilder responseBuilder, int lineCounter, StringBuilder lineBuilder)
            {
            if (lineBuilder.Length < 1) return lineCounter; // Don't add empty lines
            ++lineCounter;
            var key = lineCounter.ToString("D3");
            var value = lineBuilder.ToString();
            responseBuilder.AddPayloadItem(key, value);
            lineBuilder.Clear();
            Dbg.Trace("Added payload item key=" + key + " value=" + value, Source.WeatherConsole);
            return lineCounter;
            }

        /// <summary>
        ///     Gets the received bytes as UTF8 and truncates the receive buffer.
        /// </summary>
        /// <returns>
        ///     A <see cref="StringBuilder" /> containing the converted bytes.
        /// </returns>
        /// <remarks>
        ///     NOTE: Originally we used Encoding.UTF8.GetChars() and returned an char[] - but this threw unexplained
        ///     exceptions somewhere in native code, so we switched to converting each character one at a time, inside a
        ///     try/catch block and a StringBuilder seemed the easiest way to collect the results because of the way it
        ///     grows dynamically. Strangely, with the same input, we no longer see any exceptions.
        /// </remarks>
        StringBuilder GetReceivedBytesAsUtf8()
            {
            if (bufferHead == 0)
                {
                return new StringBuilder(); // Empty array - we don't like nulls.
                }
            byte[] converterBuffer;
            lock (receiveBuffer)
                {
                converterBuffer = new byte[bufferHead];
                Array.Copy(receiveBuffer, converterBuffer, bufferHead);
                bufferHead = 0; // Truncate the buffer
                }
            var builder = new StringBuilder();
            foreach (byte b in converterBuffer)
                {
                try
                    {
                    builder.Append(Convert.ToChar(b));
                    }
                catch (Exception ex)
                    {
                    Dbg.Trace("Exception converting byte " + b.ToString() + " to char", Source.WeatherConsole);
                    }
                }
            return builder;
            }

        void WakeupIfNecessary()
            {
            var timeSinceLastOperation = DateTime.UtcNow - timeOfLastOperation;
            if (timeSinceLastOperation > consoleSleepTime)
                {
                Wakeup();
                }
            }

        void TimestampLastOperation()
            {
            timeOfLastOperation = DateTime.UtcNow;
            }


        void ResetBuffers()
            {
            serialPort.DiscardOutBuffer();
            serialPort.DiscardInBuffer();
            serialPort.BaseStream.Flush();
            }

        /// <summary>
        ///     Writes a string to the console after adding a line terminator.
        /// </summary>
        /// <param name="data">The data to be transmitted.</param>
        public void WriteLine(string data)
            {
            WakeupIfNecessary();
            var outBuffer = Encoding.UTF8.GetBytes(data);
            serialPort.Write(outBuffer, 0, outBuffer.Length);
            serialPort.WriteByte(0x0A);
            TimestampLastOperation();
            }
        }
    }