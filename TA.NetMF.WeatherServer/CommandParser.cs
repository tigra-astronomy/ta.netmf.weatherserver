// This file is part of the TA.NetMF.WeatherServer project
// 
// Copyright � 2015 Tigra Networks., all rights reserved.
// 
// File: CommandParser.cs  Last modified: 2015-07-06@18:21 by Tim Long

using System;
using System.Text.RegularExpressions;

namespace TA.NetMF.WeatherServer
    {
    internal class CommandParser
        {
        /*
         * Regex matches command strings in format "<Dn,TT,CommandVerb=Payload>
         * D is the Device class, always 'F' for this driver.
         * n is the device index, always '1' for this driver.
         * TT is a numeric transaction ID of at least 2 digits.
         * CommandVerb is, obviously, the command verb ;-)
         * Payload is optional and is used to supply any parameter values to the command.
         * 
         * N.B. Micro Framework doesn't support named captures and will throw exceptions if they are used.
         */

        const string CommandRegex = @"<(\w\d),(\d+),([A-Za-z]\w+)(=((\d+)|(.+)))?>";
        static readonly Regex Parser = new Regex(CommandRegex);

        /// <summary>
        ///     Parses the command string and populates a <see cref="Command" /> structure.
        /// </summary>
        /// <param name="text">The text that was received from the network stream.</param>
        /// <returns>A fully populated instance of the <see cref="Command" /> value type.</returns>
        /// <exception cref="System.ArgumentException">Invalid command format</exception>
        /// <remarks>
        ///     NOTE: There are some significant differences in the regular expression implementation compared to the
        ///     "desktop" framework. Named captures are not available and the group numbering differs, so that for example,
        ///     the ReSharper regular expression validator can be used as a guide but the results will not be exactly
        ///     correct and need to be evaluated within the Micro Framework interpreter to be sure of getting the correct
        ///     results.
        /// </remarks>
        internal static Command ParseCommand(string text)
            {
            var matches = Parser.Match(text);
            if (!matches.Success)
                throw new ArgumentException("Invalid command format");
            /* Command with numeric payload has the following groups
             * Group[0] contains [<F1,234,Move=12345>]
             * Group[1] contains [F1]
             * Group[2] contains [234]
             * Group[3] contains [Move]
             * Group[4] contains [=12345]
             * Group[5] contains [12345]
             * Group[6] contains [12345]  
             * -----
             * Command with text payload has the following groups:
             * Group[0] contains [<F1,234,Nickname=Fred>]
             * Group[1] contains [F1]
             * Group[2] contains [234]
             * Group[3] contains [Nickname]
             * Group[4] contains [=Fred]
             * Group[5] contains [Fred]
             * Group[7] contains [Fred]
             * -----
             * Command with verb only (no payload) produces these groups:
             * Group[0] contains [<F1,234,Stop>]
             * Group[1] contains [F1]
             * Group[2] contains [234]
             * Group[3] contains [Stop]
             */

            // The first 3 groups are always present for a successful match, so get them first.
            var deviceAddress = matches.Groups[1].Value;
            var transaction = int.Parse(matches.Groups[2].Value);
            var verb = matches.Groups[3].Value;

            // Set defaults for the remaining values in case they are not present. We don't want any nulls.
            var position = Command.NoPosition;
            var data = string.Empty;

            // The presence of Group[7] indicates a text payload and negates the possibility of a numeric payload
            if (matches.Groups.Count >= 7 && matches.Groups[7].Success)
                {
                data = matches.Groups[7].Value;
                }
            else
                {
                // The presence of Group[6] indicates a numeric payload but ONLY if there was no text payload
                if (matches.Groups.Count >= 6 && matches.Groups[6].Success)
                    position = int.Parse(matches.Groups[6].Value);
                }
            var source = matches.Groups[0].Success ? matches.Groups[0].Value : text;
            var command = new Command(deviceAddress, transaction, verb, data, position, source);
            return command;
            }
        }
    }