using System.IO;

namespace TA.NetMF.WeatherServer
    {
    public interface IWeatherConsole {
        void Open();

        void Close();

        /// <summary>
        ///     Try up to 3 times to wake up the console, which can take up to 1.2 seconds per attempt.
        /// </summary>
        /// <exception cref="IOException">Unable to wake up the console</exception>
        void Wakeup();

        /// <summary>
        ///     Receives a multi line response from the station directly into a <see cref="ResponseBuilder" />. Data is
        ///     received until there has been nothing received for <paramref name="quietTime" />.
        /// </summary>
        /// <param name="responseBuilder">The response builder.</param>
        /// <param name="quietTime">
        ///     The quiet time that must elapse before the response is considered to have ended.
        /// </param>
        /// <returns>ResponseBuilder.</returns>
        ResponseBuilder ReceiveResponse(ResponseBuilder responseBuilder, Timeout quietTime);

        /// <summary>
        /// Writes a line-terminated string to the console.
        /// </summary>
        /// <param name="data">The data to be transmitted.</param>
        void WriteLine(string data);
    }
    }