// This file is part of the TA.NetMF.WeatherServer project
// 
// Copyright � 2015 Tigra Networks., all rights reserved.
// 
// File: PassthroughCommandProcessor.cs  Last modified: 2015-07-06@09:17 by Tim Long

using TA.NetMF.WeatherServer.CommandTargets;

namespace TA.NetMF.WeatherServer.CommandProcessors
    {
    internal class PassthroughCommandProcessor : ICommandProcessor
        {
        readonly string address;
        readonly VantageProCommandTarget commandTarget;
        readonly IWeatherConsole console;
        const string CommandVerb = "Pass";


        public PassthroughCommandProcessor(string address, VantageProCommandTarget commandTarget, IWeatherConsole console)
            {
            this.address = address;
            this.commandTarget = commandTarget;
            this.console = console;
            }

        public string DeviceAddress
            {
            get { return address; }
            }

        public string Verb
            {
            get { return CommandVerb; }
            }

        public Response Execute(Command command)
            {
            var builder = new ResponseBuilder(command);
            console.WriteLine(command.Payload);
            var responseBuilder = console.ReceiveResponse(builder, Timeout.FromMilliseconds(1000));
            return responseBuilder.ToResponse();
            }
        }
    }