﻿using System;
using System.IO.Ports;
using TA.NetMF.WeatherServer.CommandProcessors;

namespace TA.NetMF.WeatherServer.CommandTargets
    {
    internal class VantageProCommandTarget : ICommandTarget
        {
        readonly DavisVantagePro2WeatherConsole console;
        readonly string address;

        public VantageProCommandTarget(DavisVantagePro2WeatherConsole console, string address)
            {
            this.console = console;
            this.address = address;
            }

        public ICommandProcessor[] GetCommandProcessors()
            {
            var passthrough = new PassthroughCommandProcessor(address, this, console);
            var processors = new ICommandProcessor[] { passthrough };
            return processors;

            }
        }
    }